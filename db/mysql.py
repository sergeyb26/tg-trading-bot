from mysql.connector import connect, Error
import logging

class TradingDatabase:

    def __init__(self, username: str, password: str, database: str, hostname: str = '127.0.0.1', port: int = 3306) -> None:
        self.username = username
        self.password = password
        self.database = database
        self.hostname = hostname
        self.port = port

        try:
            self.connection = connect(
                host=self.hostname,
                port=self.port,
                user=self.username,
                password=self.password,
                database=self.database,
            )
        except Error as e:
            print(e)

    def get_user_data(self, user_id: int) -> dict:
        query = f"SELECT binarium_username, binarium_password, binarium_otp, base_bet, balance FROM users WHERE tg_user_id = {user_id}"
        result = {}
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(query)
                result = cursor.fetchone()
        except Exception as e:
            logging.error(f"Unable to get user {user_id} data from database: {e}")
        finally:
            return result

    def set_user_balance(self, user_id: int, balance: int) -> bool:
        query = f"UPDATE users SET balance = {balance} WHERE tg_user_id = {user_id}"
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(query)
                self.connection.commit()
                return True
        except Exception as e:
            logging.error(f"Unable to set user {user_id} balance to {balance}: {e}")
        return False
        
    def set_user_base_bet(self, user_id: int, new_bet: int) -> bool:
        query = f"UPDATE users SET base_bet = {new_bet} WHERE tg_user_id = {user_id}"
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(query)
                self.connection.commit()
                return True
        except Exception as e:
            logging.error(f"Unable to set user {user_id} base bet to {new_bet}: {e}")
        return False