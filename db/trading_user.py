import time
from db.mysql import TradingDatabase
from binarium_driver import BinariumDriver

class TradingUser:
    telegram_user_id = 0
    binarium = None
    binarium_username = None
    binarium_password = None
    binarium_otp = None
    base_bet = 0
    balance = 0

    def __init__(self, db: TradingDatabase, tg_user_id: int):
        self.telegram_user_id = tg_user_id
        self.db = db
        self.tg_user_id = tg_user_id
        dict = self.db.get_user_data(tg_user_id)
        # dict: binarium_username, binarium_password, binarium_otp, base_bet, balance
        if len(dict) > 0 :
            self.binarium_username = dict[0]
            self.binarium_password = dict[1]
            self.binarium_otp = dict[2]
            if dict[3] is not None:
                self.base_bet = int(dict[3])
            if dict[4] is not None:
                self.balance = int(dict[4])

    def is_valid(self) -> bool:
        if self.binarium_username is None or self.binarium_password is None:
            return False
        return True

    def start_binarium(self, url:str, webDriverPath: str):
        """
        Open the browser and authorize in binarium website
        """
        self.binarium = BinariumDriver(self.binarium_username, self.binarium_password, url, webDriverPath)
        self.binarium.BeginSession()

    def stop_binarium(self):
        """
        Close the browser
        """
        self.binarium.EndSession()

    def get_balance(self) -> None:
        """
        Returns user's balance
        """
        if not self.binarium is None and self.binarium.SessionStarted:
            self.balance = self.binarium.GetWalletBalance()
            self.db.set_user_balance(self.telegram_user_id, self.balance)
        return self.balance

    def set_demo_mode(self, toggle: bool) -> None:
        """
        Set demo mode on or off
        """
        if not self.binarium is None and self.binarium.SessionStarted:
            self.binarium.SwitchToDemoAccount(toggle)
        else:
            raise Exception("Binarium session is not started")

    def set_trading_pair(self, pair: str) -> None:
        """
        Set trading pair
        """
        if not self.binarium is None and self.binarium.SessionStarted:
            self.binarium.ChoosePair(pair)
        else:
            raise Exception("Binarium session is not started")

    def set_base_bet(self, base_bet: int) -> None:
        """
        Set a base bet for the account
        """
        self.base_bet = base_bet
        self.db.set_user_base_bet(self.telegram_user_id, base_bet)
        if not self.binarium is None and self.binarium.SessionStarted:
            self.binarium.SetBetValue(base_bet)

    def set_expire_time(self, exp_time: int) -> None:
        """
        Set a base bet for the account
        """
        if not self.binarium is None and self.binarium.SessionStarted:
            try:
                self.binarium.SetExpirationTime(exp_time)
            except:
                time.sleep(5)
                self.binarium.SetExpirationTime(exp_time)