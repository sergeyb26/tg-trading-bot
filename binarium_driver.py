import re
import logging
import platform
from datetime import datetime
from enum import Enum
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

class OptionDirection(Enum):
    PUT = -1
    CALL = 1

class BinariumError(Exception):
    pass

class BinariumDriver:
    SessionStarted = False

    def __init__(self, login, password, url = "https://binarium.digital/ru", webDriverPath = "/usr/bin/chromedriver"):
        self.binariumLogin = login
        self.binariumPassword = password
        self.binariumUrl = url
        self.webDriverPath = webDriverPath
        self.browser = None

    def BeginSession(self):
        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications" : 2}
        chrome_options.add_experimental_option("prefs",prefs)
        self.browser = webdriver.Chrome(self.webDriverPath, chrome_options=chrome_options)
        self.browser.get(self.binariumUrl)

        # Check if cookie notification is shown
        try:
            cookieCloseButton = WebDriverWait(self.browser, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, "app-cookie-request button"))
            )
            actions = ActionChains(self.browser)
            actions.move_to_element(cookieCloseButton).click().perform()
        except:
            pass
        self.SessionStarted = True

        # Click on login button
        try:
            loginTab = WebDriverWait(self.browser, 30).until(
                EC.presence_of_element_located((By.XPATH, "/html/body/app-client/div/ng-component/mat-sidenav-container/mat-sidenav-content/div[5]/app-start-sidebar/div/div[1]/div[2]/div/div/div/div/a[1]"))
                )
            actions = ActionChains(self.browser)
            actions.move_to_element(loginTab).click().perform()
        except Exception as err:
            logging.error(err)
            raise BinariumError("Unable to login (can't find login tab)")

        # Fill login form
        try:
            emailinput = WebDriverWait(self.browser, 10).until(
                EC.presence_of_element_located((By.XPATH, "/html/body/app-client/div/ng-component/mat-sidenav-container/mat-sidenav-content/div[5]/app-start-sidebar/div/div[2]/app-auth-form/div/div/form/app-email-control/mat-form-field/div/div[1]/div/input"))
            )
            emailinput.send_keys(self.binariumLogin)
            passwordinput = self.browser.find_element(By.XPATH, "/html/body/app-client/div/ng-component/mat-sidenav-container/mat-sidenav-content/div[5]/app-start-sidebar/div/div[2]/app-auth-form/div/div/form/app-password-control/mat-form-field/div/div[1]/div/input")
            passwordinput.send_keys(self.binariumPassword)
            enterButton = self.browser.find_element(By.XPATH, "/html/body/app-client/div/ng-component/mat-sidenav-container/mat-sidenav-content/div[5]/app-start-sidebar/div/div[2]/app-auth-form/div/div/form/button")
            actions = ActionChains(self.browser)
            actions.move_to_element(enterButton).click().perform()
        except Exception as err:
            logging.error(err)
            raise BinariumError("Unable to login (can't fill the form)")

        # Try close Two-factor authentication popup
        try:
            twoFactorCloseButton = WebDriverWait(self.browser, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, ".offer .offer__close"))
            )
            actions = ActionChains(self.browser)
            actions.move_to_element(twoFactorCloseButton).click().perform()
        except:
            pass

    # Closes modal window
    def CloseModal(self):
        try:
            modalWindow = self.browser.find_element_by_class_name('t-modal-container')
            if modalWindow.is_displayed():
                self.browser.find_element_by_css_selector("div[a-test=largestProfitabilityClose]").click()
        except:
            pass

    def CloseChat(self):
        """
        Closes chat window
        """
        try:
            chatWindow = WebDriverWait(self.browser, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, ".lt-chat"))
            )
            chatCloseButton = chatWindow.find_element(By.CSS_SELECTOR, "button.lt-wrapper-close")
            action = ActionChains(self.browser)
            action.move_to_element(chatCloseButton).click().perform()
        except:
            pass

    def CloseSpam(self):
        """
        Closes spam window
        """
        self.CloseModal()
        self.CloseChat()

    def GetWalletBalance(self):
        """
        Returns wallet balance
        """
        if not self.SessionStarted:
            raise BinariumError("Session not started")
        wallet = WebDriverWait(self.browser, 30).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, "div[a-test=fullBalance]"))
        )
        return float(re.sub('[^0-9^\.]', '', wallet.text))

    def SwitchToDemoAccount(self, toggle: bool):
        """
        Switches to demo account
        """
        changeWalletButton = self.browser.find_element(By.CLASS_NAME, "wallet-menu-total__name")
        actions = ActionChains(self.browser)
        actions.move_to_element(changeWalletButton).click().perform()
        className = "--demo" if toggle else "--real"
        demoAccountButton = self.browser.find_element(By.CSS_SELECTOR, f"div.wallet-account.{className} div.wallet-account__button")
        actions = ActionChains(self.browser)
        actions.move_to_element(demoAccountButton).click().perform()

    def ChoosePair(self, pair):
        """
        Choose a pair to trade on

        :param pair: Pair to choose
        """
        # Check if asset selector is already opened
        assets = self.browser.find_elements(By.CLASS_NAME, 'am-asset')
        if len(assets) > 0:
            el = self.browser.find_element(By.CSS_SELECTOR, '.select-popup-wrapper.--active')
            actions = ActionChains(self.browser)
            actions.move_to_element(el).click().perform()

        # Search if the pair is already opened
        currentPair = self.browser.find_element(By.CSS_SELECTOR, ".header__chart .chart-tabs__item .chart-tab__header div")
        if currentPair.text == pair:
            return

        # divs = self.browser.find_elements(By.CSS_SELECTOR, '.header__chart .chart-tabs__item')
        # for d in divs:
        #     name = d.find_element(By.CLASS_NAME, 'chart-tab__header')
        #     if d.find_element(By.CLASS_NAME, 'chart-tab__header').text == pair:
        #         element = d.find_element(By.CLASS_NAME, 'chart-tab__body')
        #         actions = ActionChains(self.browser)
        #         actions.move_to_element(element).click().perform()
        #         break

        # Open asset in the first tab
        firstTab = self.browser.find_element(By.CSS_SELECTOR, ".chart-tabs__item .chart-tab__header")
        actions = ActionChains(self.browser)
        actions.move_to_element(firstTab).click().perform()
        group = self.browser.find_element(By.CLASS_NAME, "am-group")
        assets = group.find_elements(By.CLASS_NAME, 'am-asset')
        for asset in assets:
            name = asset.find_element(By.CSS_SELECTOR, '.am-asset__name span').get_attribute('textContent')
            if name == pair:
                scrollTo = int(asset.get_attribute('offsetTop')) - int(group.get_attribute('offsetTop'))
                self.browser.execute_script("document.getElementsByClassName('am-content__assets')[0].scrollTo(0, {})".format(scrollTo))
                profits = asset.find_elements(By.CLASS_NAME, 'am-asset__profit')
                for profit in profits:
                    try:
                        percents = profit.find_element(By.TAG_NAME, 'span')
                    except Exception as err:
                        continue
                    actions = ActionChains(self.browser)
                    actions.move_to_element(profit).click().perform()
                    break
                break

    def SetBetValue(self, val: int) -> None:
        """
        Sets a bet value

        :param val: Bet value
        """
        betValueControl = self.browser.find_element(By.CLASS_NAME, 'chart-bet-control')
        betValue = betValueControl.find_element(By.CSS_SELECTOR, '.chart-bet-control__value textarea').get_attribute('value')
        if int(betValue) == val:
            return
    
        try:
            betValueInput2 = self.browser.find_element(By.CSS_SELECTOR, '.select-popup-wrapper.--active .chart-bet-input textarea')
        except:
            actions = ActionChains(self.browser)
            actions.move_to_element(betValueControl).click().perform()
            betValueInput2 = self.browser.find_element(By.CSS_SELECTOR, '.select-popup-wrapper.--active .chart-bet-input textarea')

        changeBetAction = ActionChains(self.browser)
        changeBetAction.click(betValueInput2)
        if platform.system() == 'Darwin':
            changeBetAction.key_down(Keys.COMMAND).send_keys('a').key_up(Keys.COMMAND).send_keys(Keys.DELETE)
        else:
            changeBetAction.key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL).send_keys(Keys.DELETE)
        changeBetAction.send_keys('{}'.format(val))
        changeBetAction.move_to_element(betValueControl).click().perform()
        changeBetAction.perform()

    def SetExpirationTime(self, val: str) -> None:
        """
        Sets a bet expiration time

        :param val: Bet expiration time
        """
        currentValue = self.browser.find_element(By.CSS_SELECTOR, '.chart-aside__expiration .chart-control__value span[a-test=currentExpiration]').get_attribute('textContent')
        if currentValue != "":
            currentDate = datetime.strptime(currentValue, '%H:%M')
            betDate = datetime.strptime(val, "%H:%M")
            if currentDate == betDate:
                return
            minutes = int((betDate - currentDate).total_seconds()/60.0)
            controls = None
            if minutes >= 0:
                controls = self.browser.find_element(By.CSS_SELECTOR, '.chart-aside__discrets .spinners .--inc')
            else:
                controls = self.browser.find_element(By.CSS_SELECTOR, '.chart-aside__discrets .spinners .--dec')

            actions = ActionChains(self.browser)
            actions.move_to_element(controls)
            for i in range(0, abs(minutes)):
                actions.click()
            actions.perform()

            currentValue = self.browser.find_element(By.CSS_SELECTOR, '.chart-aside__expiration .chart-control__value span[a-test=currentExpiration]').get_attribute('textContent')
            currentDate = datetime.strptime(currentValue, '%H:%M')
            if currentDate == betDate:
                return

        # If set time fail, try another method
        try:
            expItems = self.browser.find_elements(By.CSS_SELECTOR, '.select-popup-wrapper.--active .chart-dropdown-item')
            if len(expItems) == 0:
                raise Exception('')
        except:
            expBlock = self.browser.find_element(By.CSS_SELECTOR, '.chart-aside__controls .chart-control__value')
            actions = ActionChains(self.browser)
            actions.move_to_element(expBlock).click().perform()
            expItems = self.browser.find_elements(By.CSS_SELECTOR, '.select-popup-wrapper.--active .chart-dropdown-item')

        for expItem in expItems:
            elementValue = expItem.find_element(By.CLASS_NAME, 'chart-dropdown-item__text').get_attribute('textContent')
            if elementValue == val:
                actions = ActionChains(self.browser)
                actions.move_to_element(expItem).click().perform()
                return
        raise BinariumError('Expiration time {} not found'.format(val))

    def PlaceBet(self, pair: str, time: str, direction: OptionDirection, bet: int):
        """
        Places a bet
        """
        if not self.SessionStarted:
            raise BinariumError("Trade session not started")
        self.CloseSpam()
        self.ChoosePair(pair)
        self.SetBetValue(bet)
        self.SetExpirationTime(time)
        if direction == OptionDirection.PUT:
            button = self.browser.find_element(By.CSS_SELECTOR, '[atest=buttonPut]')
        elif direction == OptionDirection.CALL:
            button = self.browser.find_element(By.CSS_SELECTOR, '[atest=buttonCall]')
        actions = ActionChains(self.browser)
        actions.move_to_element(button).click().perform()

    def EndSession(self):
        """
        Ends the trading session
        """
        self.browser.quit()
        self.SessionStarted = False
