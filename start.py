import logging
import yaml
from tradingbot import TradingTelegramBot
from db.mysql import TradingDatabase

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

if __name__ == '__main__':
    ## Read configuration from a file
    try:
        configFile = yaml.safe_load(open("config.yaml", "r"))
    except Exception as exc:
        logging.fatal("Unable to read config: {}".format(exc))
        exit(1)

    if not 'telegram' in configFile or not 'token' in configFile['telegram']:
        logging.fatal("No telegram bot token in config.yaml")
        exit(2)

    if (not 'binarium' in configFile 
        or not 'url' in configFile['binarium'] 
        or not 'driverpath' in configFile['binarium']
    ):
        logging.fatal("No binarium config in config.yaml")
        exit(2)

    tradingDb = TradingDatabase(
        username = configFile['database']['username'],
        password = configFile['database']['password'],
        database = configFile['database']['database'],
        hostname = configFile['database']['hostname'],
        port = configFile['database']['port']
    )

    tradingBot = TradingTelegramBot(token=configFile["telegram"]["token"], db=tradingDb, config=configFile)
    tradingBot.start()
