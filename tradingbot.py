from contextvars import Context
import os
from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler
from db.trading_user import TradingUser
from db.mysql import TradingDatabase

class TradingTelegramBot:

    users = {}

    def __init__(self, token: str, db: TradingDatabase, config: dict) -> None:
        """
        Initialize the bot
        """
        self.token = token
        self.db = db
        self.config = config

    def user_handler(fnc):
        """
        Decorator for checking authorization of the user. Gets it from the database and store in dict
        """
        async def inner(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
            user_id = update.effective_chat.id
            if not user_id in self.users.keys():
                user = TradingUser(self.db, user_id)
                if user.is_valid():
                    self.users[user_id] = user
            else:
                user = self.users[user_id]
            
            if user is None or not user.is_valid():
                await context.bot.send_message(chat_id=user_id, text=f"You are not registered yet.")
                return
            await fnc(self, update, context, user)
        return inner

    async def get_id_handler(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        """
        Return user_id on /id command
        """
        await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Your ID: {update.effective_chat.id}")

    @user_handler
    async def session_start_handler(self, update: Update, context: ContextTypes.DEFAULT_TYPE, user: TradingUser) -> None:
        """
        Start a new trading session
        """
        msg = await update.message.reply_text(f"Starting session for {user.binarium_username}")
        try:
            user.start_binarium(self.config['binarium']['url'], self.config['binarium']['driverpath'])
            await msg.edit_text("Session started")
        except Exception as e:
            await msg.edit_text(f"Error starting session: {e}")

    @user_handler
    async def session_stop_handler(self, update: Update, context: ContextTypes.DEFAULT_TYPE, user: TradingUser) -> None:
        """
        Stop the trading session
        """
        msg = await update.message.reply_text(f"Stopping trading session")
        try:
            user.stop_binarium()
            await msg.edit_text("Session stopped")
        except Exception as e:
            await msg.edit_text(f"Error stopping session: {e}")

    @user_handler
    async def balance_handler(self, update: Update, context: ContextTypes.DEFAULT_TYPE, user: TradingUser) -> None:
        """
        Get the balance of the user
        """
        try:
            balance = user.get_balance()
            await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Your balance: {balance}")
        except Exception as e:
            await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Error getting balance: {e}")

    @user_handler
    async def switch_demo_handler(self, update: Update, context: ContextTypes.DEFAULT_TYPE, user: TradingUser) -> None:
        """
        Switch to demo mode
        """
        msgArray = update.message.text.split(' ')
        if len(msgArray) > 1:
            demo = msgArray[1]
            if demo == '1':
                user.set_demo_mode(True)
                try:
                    await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Demo mode on")
                except Exception as e:
                    await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Error switching to demo mode: {e}")
            elif demo == '0':
                user.set_demo_mode(False)
                await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Demo mode off")
            else:
                await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Unknown argument {demo}")
        else:
            await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Syntax: `/demo [0/1]")

    @user_handler
    async def choose_pair_handler(self, update: Update, context: ContextTypes.DEFAULT_TYPE, user: TradingUser) -> None:
        """
        Choose a pair to trade
        """
        msgArray = update.message.text.split(' ')
        if len(msgArray) > 1:
            try:
                pair = msgArray[1]
                user.set_trading_pair(pair)
                await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Swithced to {pair}")
            except Exception as e:
                await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Error switching pair: {e}")
        else:
            await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Syntax: /pair [pair]")

    @user_handler
    async def set_base_bet(self, update: Update, context: ContextTypes.DEFAULT_TYPE, user: TradingUser) -> None:
        """
        Set the base bet
        """
        msgArray = update.message.text.split(' ')
        if len(msgArray) > 1:
            try:
                bet = int(msgArray[1])
                user.set_base_bet(bet)
                await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Base bet set to {bet}")
            except Exception as e:
                await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Error setting base bet: {e}")
        else:
            await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Syntax: /bet [bet]")

    @user_handler
    async def set_expiration_time(self, update: Update, context: ContextTypes.DEFAULT_TYPE, user: TradingUser) -> None:
        """
        Set the base bet
        """
        msgArray = update.message.text.split(' ')
        if len(msgArray) > 1:
            try:
                time = msgArray[1]
                user.set_expire_time(time)
                await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Expiration time is set to {time}")
            except Exception as e:
                await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Error setting expiration time: {e}")
        else:
            await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Syntax: /time [time]")

    def start(self) -> None:
        """
        Start the bot
        """
        self.bot = ApplicationBuilder().token(self.token).build()
        self.bot.add_handler(CommandHandler('id', self.get_id_handler))
        self.bot.add_handler(CommandHandler('start', self.session_start_handler))
        self.bot.add_handler(CommandHandler('stop', self.session_stop_handler))
        self.bot.add_handler(CommandHandler('balance', self.balance_handler))
        self.bot.add_handler(CommandHandler('demo', self.switch_demo_handler))
        self.bot.add_handler(CommandHandler('pair', self.choose_pair_handler))
        self.bot.add_handler(CommandHandler('bet', self.set_base_bet))
        self.bot.add_handler(CommandHandler('time', self.set_expiration_time))
        self.bot.run_polling()
